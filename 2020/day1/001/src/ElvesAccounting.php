<?php
namespace Advent\day1\one;


class ElvesAccounting
{
    /**
     * @var array
     */
    protected static array $input = [];

    /**
     * @return array
     */
    public static function getInput() : array
    {
        return self::$input;
    }

    /**
     * @param array $input
     */
    public static function setInput(array $input)
    {
        self::$input = $input;
    }


    function findSumPairs($sum) : array
    {
        $sums = [];
        $n = count(self::$input);
        for ($i = 0; $i < $n; $i++) {
            for ($j = $i + 1; $j < $n; $j++) {
                $a = self::$input[$i];
                $b = self::$input[$j];
                if ($a+$b == $sum) {
                    $numbers = [$a,$b];
                    sort($numbers);
                    return $numbers;
                }
            }
        }

        return $sums;
    }

    function productOfArray($array) : int
    {
        return array_product($array);
    }

}

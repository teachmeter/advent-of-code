<?php 
require_once __DIR__ . '/../vendor/autoload.php';

$input = json_decode(file_get_contents(__DIR__ . '/input.json'), true);
$accounting = new Advent\day1\one\ElvesAccounting();
$accounting::setInput($input);

$products = $accounting->findSumPairs(2020);
$product = $accounting->productOfArray($products);

echo implode(', ', $products) . "\n";
echo $product  . "\n";;
<?php

use PHPUnit\Framework\TestCase;

class AccountingTests extends TestCase
{
    public function testAssertHasInput()
    {
        $accounting = new \Advent\day1\one\ElvesAccounting();

        $accounting->setInput([1,2,3]);

        $this->assertEquals($accounting::getInput(), [1,2,3]);

    }

    public function testAssertReturnsPairs()
    {
        $accounting = new \Advent\day1\one\ElvesAccounting();
        $accounting::setInput([1,1,2,2]);

        $pairs = $accounting->findSumPairs(3);

        $this->assertIsArray($pairs);
    }
}

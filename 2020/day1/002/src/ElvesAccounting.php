<?php
namespace Advent\day1\two;


class ElvesAccounting
{
    /**
     * @var array
     */
    protected static array $input = [];

    /**
     * @return array
     */
    public static function getInput() : array
    {
        return self::$input;
    }

    /**
     * @param array $input
     */
    public static function setInput(array $input)
    {
        self::$input = $input;
    }


    function findSumTriplets($sum) : array
    {
        $sums = [];
        $n = count(self::$input);
        for ($i = 0; $i < $n; $i++) {
            for ($j = $i + 1; $j < $n; $j++) {
                for ($k = $i + 2; $k < $n; $k++) {
                    $a = self::$input[$i];
                    $b = self::$input[$j];
                    $c = self::$input[$k];
                    if ($a+$b+$c == $sum) {
                        $numbers = [$a,$b,$c];
                        sort($numbers);
                        return $numbers;
                    }
                }
            }
        }

        return $sums;
    }

    function productOfArray($array) : int
    {
        return array_product($array);
    }

}

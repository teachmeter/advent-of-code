<?php


namespace Advent\day2\one;


class Password
{
    public static array $input = [];

    public function getRowRange($string) : array
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }
        $string = trim($string);
        $stringArray = explode(' ', $string);
        $rangeString = $stringArray[0];
        $rangeArray = explode('-', $rangeString);

        if (empty($rangeArray)) {
            throw new \Exception('Could not determine range');
        }

        $range = [
            'min' => min($rangeArray),
            'max' => max($rangeArray)
        ];

        return $range;
    }

    public function getRowCharacter($string) : string
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }

        $stringArray = explode(' ', $string);
        $character = str_replace(':', '', $stringArray[1]);

        return $character;
    }

    public function getRowPassword($string) : string
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }

        $stringArray = explode(' ', $string);
        $password = $stringArray[2];

        return $password;
    }

    public function getCountCharactersInString($password, $char) : int
    {
        $count = 0;

        for($i=0; $i<strlen($password); $i++) {
            if($char === $password[$i]) {
                $count++;
            }
        }

        return $count;
    }

    public function passwordIsValid($string) : bool
    {
        try {
            $range = $this->getRowRange($string);
            $password = $this->getRowPassword($string);
            $char = $this->getRowCharacter($string);
            $count = $this->getCountCharactersInString($password, $char);
            $inRange = $this->inRange($count, $range['min'], $range['max']);
        } catch (\Exception $e) {
            return false;
        }

        return $inRange;
    }

    public function inRange(int $number, $min = 0, $max = INF)
    {
        return $number >= $min && $number <= $max;
    }


}
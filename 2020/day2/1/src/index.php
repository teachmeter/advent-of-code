<?php

require_once __DIR__ . '/../vendor/autoload.php';

$input = json_decode(file_get_contents(__DIR__ . '/input.json'), true);

$validCount = 0;

$pwd = new \Advent\day2\one\Password();

foreach ($input as $k => $passwordRow) {
    if($pwd->passwordIsValid($passwordRow) === true ) {
        $validCount++;
    }
}

echo sprintf('Valid number of passwords %d', $validCount). "\n";
die();
<?php


use Advent\day2\one\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{

    public static array $testInput
        = [
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc",
        ];

    public function testGetRange()
    {
        $pwd = new Password(self::$testInput);

        $range = $pwd->getRowRange((self::$testInput)[0]);

        $this->assertIsArray($range);
        $this->assertEquals(['min' => 1, 'max'=> 3], $range);
    }

    public function testGetRowCharacter()
    {
        $pwd = new Password();

        $character = $pwd->getRowCharacter((self::$testInput)[0]);

        $this->assertIsString($character);
        $this->assertEquals('a', $character);
    }

    public function testGetRowPassword()
    {
        $pwd = new Password();

        $password = $pwd->getRowPassword((self::$testInput)[0]);

        $this->assertIsString($password);
        $this->assertEquals('abcde', $password);
    }

    public function testCountCharactersInString()
    {
        $pwd = new Password();

        $count = $pwd->getCountCharactersInString('ccccccccc', 'c');

        $this->assertIsInt($count);
        $this->assertEquals(9, $count);
    }

    public function testPasswordIsValid()
    {
        $pwd = new Password();
        $isValid = $pwd->passwordIsValid(self::$testInput[0]);

        $this->assertIsBool($isValid);
        $this->assertEquals(true, $isValid);
    }

    public function testInRange()
    {
        $pwd = new Password();
        $inRange = $pwd->inRange(2, 1, 3);

        $this->assertIsBool($inRange);
        $this->assertEquals(true, $inRange);
    }

    public function testNotInRange()
    {
        $pwd = new Password();
        $inRange = $pwd->inRange(4, 1, 3);

        $this->assertIsBool($inRange);
        $this->assertEquals(false, $inRange);
    }

}

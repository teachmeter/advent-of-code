<?php


namespace Advent\day2\two;


class Password
{
    public static array $input = [];

    public function getRowRange($string) : array
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }
        $string = trim($string);
        $stringArray = explode(' ', $string);
        $rangeString = $stringArray[0];
        $rangeArray = explode('-', $rangeString);

        if (empty($rangeArray)) {
            throw new \Exception('Could not determine range');
        }

        $range = [
            'min' => min($rangeArray),
            'max' => max($rangeArray)
        ];

        return $range;
    }

    public function getPositions($string): array
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }
        $string = trim($string);
        $stringArray = explode(' ', $string);
        $rangeString = $stringArray[0];
        $rangeArray = explode('-', $rangeString);
        
        $rangeArray = array_map(function($element) {
            return $element -= 1;
        }, $rangeArray);

        return $rangeArray;
    }

    public function getRowCharacter($string) : string
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }

        $stringArray = explode(' ', $string);
        $character = str_replace(':', '', $stringArray[1]);

        return $character;
    }

    public function getRowPassword($string) : string
    {
        if (empty($string)) {
            throw new \Exception('String empty');
        }

        $stringArray = explode(' ', $string);
        $password = $stringArray[2];

        return $password;
    }

    public function getCountCharactersInString($password, $char) : int
    {
        $count = 0;

        for($i=0; $i<strlen($password); $i++) {
            if($char === $password[$i]) {
                $count++;
            }
        }

        return $count;
    }

    public function passwordIsValid($string) : bool
    {
        try {
            $range = $this->getRowRange($string);
            $password = $this->getRowPassword($string);
            $char = $this->getRowCharacter($string);
            $positions = $this->getPositions($string);

            var_dump($char);
            $char1AtPosition = $this->characterAtPosition($password, $char, $positions[0]);
            $char2AtPosition = $this->characterAtPosition($password, $char, $positions[1]);

            if ($char1AtPosition === true && $char2AtPosition === true) {
                echo "both true invalid \n";
                return false;
            } elseif($char1AtPosition === true && $char2AtPosition === false)  {
                echo "one of true valid 1 \n";
                return true;
            } elseif ($char2AtPosition === true && $char1AtPosition === false) {
                echo "one of true valid 2 \n";
                return true;
            } elseif( $char1AtPosition === false && $char2AtPosition === false) {
                echo "both false invalid \n";
                return false;
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return false;
    }


    public function characterAtPosition($string, $character, $position) : bool
    {
        $stringArray = str_split($string);

        return $stringArray[$position] === $character;
    }
}
<?php


use Advent\day2\two\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{

    public static array $testInput
        = [
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc",
        ];

    public function testGetRange()
    {
        $pwd = new Password();

        $range = $pwd->getRowRange((self::$testInput)[0]);

        $this->assertIsArray($range);
        $this->assertEquals(['min' => 1, 'max'=> 3], $range);
    }

    public function testGetRowCharacter()
    {
        $pwd = new Password();

        $character = $pwd->getRowCharacter((self::$testInput)[0]);

        $this->assertIsString($character);
        $this->assertEquals('a', $character);
    }

    public function testGetRowPostitions() {
        $pwd = new Password();
        $positions = $pwd->getPositions((self::$testInput)[0]);

        $this->assertEquals($positions, [0, 2]);
    }

    public function testGetRowPassword()
    {
        $pwd = new Password();

        $password = $pwd->getRowPassword((self::$testInput)[0]);

        $this->assertIsString($password);
        $this->assertEquals('abcde', $password);
    }

    public function testCountCharactersInString()
    {
        $pwd = new Password();

        $count = $pwd->getCountCharactersInString('ccccccccc', 'c');

        $this->assertIsInt($count);
        $this->assertEquals(9, $count);
    }

    public function testPasswordIsValid()
    {
        $pwd = new Password();

        $isValid = $pwd->passwordIsValid(self::$testInput[0]);
        $this->assertIsBool($isValid);
        $this->assertTrue($isValid);
    }

    public function testPasswordIsInvalid()
    {
        $pwd = new Password();
        $isValid = $pwd->passwordIsValid(self::$testInput[1]);

        $this->assertIsBool($isValid);
        $this->assertFalse($isValid);
    }

    public function testPasswordIsValid2()
    {
        $pwd = new Password();

        $isValid = $pwd->passwordIsValid(self::$testInput[2]);
        $this->assertIsBool($isValid);
        $this->assertFalse($isValid);
    }



    public function testCharacterAtStringPosition()
    {
        $string = 'abcd';

        $pwd = new Password();
        $b_at_second = $pwd->characterAtPosition($string, 'b', 1);

        $this->assertTrue($b_at_second);
    }

}

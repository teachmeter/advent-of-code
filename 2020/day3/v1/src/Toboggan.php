<?php


namespace Advent\day3\v1;


use PHPUnit\Util\Exception;

class Toboggan
{
    protected static string $mapPart = '';

    private static array $map = [];

    public int $x = 0;
    public int $y = 0;

    public int $rightMove = 1;
    public int $downMove = 1;

    public int $trees = 0;

    /**
     * Toboggan constructor.
     *
     * @param int $rightMove
     * @param int $downMove
     */
    public function __construct(int $rightMove, int $downMove)
    {
        $this->rightMove = $rightMove;
        $this->downMove = $downMove;
    }


    /**
     * @return array
     */
    public static function getMap(): array
    {
        return self::$map;
    }

    /**
     * @param array $map
     */
    public static function setMap(array $map): void
    {
        self::$map = $map;
    }

    /**
     * @return string
     */
    public static function getMapPart(): string
    {
        return self::$mapPart;
    }

    /**
     * @param string $mapPart
     */
    public static function setMapPart(string $mapPart): void
    {
        self::$mapPart = $mapPart;
    }


    public function generateMap()
    {
        $baseMapString = self::getMapPart();

        $mapRows = explode("\n", $baseMapString);
        $mapArray = array_map(
            function ($row) {
                return str_split($row);
            },
            $mapRows
        );

        $rowCount = count($mapArray);

        self::setMap($mapArray);


        while (count(self::getMap()[0]) <= ($rowCount+1) * $this->rightMove) {
            $this->extendMap();
        }
    }

    public function extendMap($times = 1)
    {
        $map = self::getMap();

        foreach ($map as $row_index => $row) {
            $map[$row_index] = str_split(str_repeat(implode('', $row), $times + 1));
        }

        self::setMap($map);
    }

    public static function dumpMap()
    {
        echo "<pre>";
        print_r(self::getMap());
        echo "</pre>";
    }


    public function traverseRight()
    {
        $this->x += $this->rightMove;
    }

    public function traverseDown()
    {
        $this->y += $this->downMove;
    }

    public function getMapValueAtCurrentPosition() : string
    {
        if (!isset(self::getMap()[$this->y][$this->x]) ) {
            throw new Exception('Position doesn\'t exist ' . $this->y . ',' . $this->x);
        }
        return self::getMap()[$this->y][$this->x];
    }


    public function getMapValueAtPosition($x, $y) : string
    {
        return self::getMap()[$y][$x];
    }

    public function getMapRows() : int
    {
        return count(self::getMap());
    }

    public function slide()
    {
        $stepsToDo = $this->getMapRows();
//        echo sprintf('steps to do %d', $stepsToDo);
        for ($currentStep = 0; $currentStep < $stepsToDo; $currentStep++) {
//            echo sprintf('step %d encountered %s', $currentStep, $this->getMapValueAtCurrentPosition()) . "\n";
            try {
                if ($this->getMapValueAtCurrentPosition() === '#') {
                    $this->trees += 1;
                }
                $this->traverseRight();
                $this->traverseDown();
            } catch (\Exception $e) {
                break;
            }

        }
    }

    public function getEncounteredTrees() : int
    {
        return $this->trees;
    }

}
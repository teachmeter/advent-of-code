<?php

require_once __DIR__ . '/../vendor/autoload.php';

$string = '..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#';

$patterns = [
    [1,1],
    [3,1],
    [5,1],
    [7,1],
    [1,2]
];

$results = [];

foreach ($patterns as $k => $pattern) {
    $toboggan = new \Advent\day3\v1\Toboggan($pattern[0],$pattern[1]);
    $toboggan::setMapPart(file_get_contents(__DIR__ . '/input.txt'));
    $toboggan->generateMap();
    $toboggan->slide();
    $trees = $toboggan->getEncounteredTrees();
    echo sprintf('pattern (r%d,d%d)|trees encountered: %d ', $pattern[0],$pattern[1],$trees) . "<br>";
    $results[] = $trees;
}

echo "product: " . array_product($results);





//
//echo "map rows: " . $toboggan->getMapRows() . "\n";
//echo "start: ";
//echo $toboggan->getMapValueAtCurrentPosition() . "\n";
//
//echo "step1: ";
//$toboggan->traverseRight();
//$toboggan->traverseDown();
//echo $toboggan->getMapValueAtCurrentPosition() . "\n";


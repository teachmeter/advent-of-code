<?php

namespace Advent\day3\v1\tests;

use Advent\day3\v1\Toboggan;
use PHPUnit\Framework\TestCase;

class TobogganTest extends TestCase
{

    public Toboggan $toboggan;
    public static string $testMapPart = '..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#';

    public function setUp() : void
    {
        $toboggan = new Toboggan(3,1);
        $toboggan::setMapPart(self::$testMapPart);
        $toboggan->generateMap();

        $this->toboggan = $toboggan;
    }

    public function testCanSetMapPart()
    {
        $toboggan = new Toboggan(3,1);

        $toboggan::setMapPart(file_get_contents(__DIR__ . '/../src/input.txt'));

        $this->assertNotEmpty($toboggan::getMapPart());
        $this->assertEquals(file_get_contents(__DIR__ . '/../src/input.txt'), $toboggan::getMapPart());
    }

    public function testSlide()
    {
        $toboggan = $this->toboggan;
        $toboggan->slide();
        $trees = $toboggan->getEncounteredTrees();

        $this->assertEquals(7, $trees);
    }

    public function testTraverseRight()
    {
        $this->toboggan->traverseRight();
        $this->assertEquals(3, $this->toboggan->x);
    }


}

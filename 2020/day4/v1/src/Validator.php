<?php
namespace Advent\day4\v1;

class Validator implements ValidatorInterface
{
    public static array $required = [
        'byr',
        'iyr',
        'eyr',
        'hgt',
        'hcl',
        'ecl',
        'pid',
//        'cid',
    ];
    public function isValid(Passport $passport): bool
    {
        $validArray = [];

        foreach (self::$required as $k => $field_name) {
            $validArray[$field_name] = $this->checkRequired($field_name, $passport);

        }

        return (bool) array_product($validArray);
    }

    public function checkRequired($key, $object)
    {
        $value = $object->{'get'.ucfirst(strtolower($key))}();
        return isset($value) && !empty($value);
    }

}
<?php


namespace Advent\day4\v1;


interface ValidatorInterface
{

    public function isValid(Passport $passport) : bool;
}
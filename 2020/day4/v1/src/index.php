<?php
require_once __DIR__ . '/../vendor/autoload.php';

$input = file_get_contents(__DIR__ . '/input.txt'); 
$validator = new \Advent\day4\v1\Validator();
$security = new \Advent\day4\v1\Security($validator);

$security->setPassports($input);
$ct = 0;
foreach ($security->getPassports() as $k  => $passport) {
    if($validator->isValid($passport)) {
        $ct++;
    }
}

echo sprintf("Valid passports with optional cid: %d", $ct);


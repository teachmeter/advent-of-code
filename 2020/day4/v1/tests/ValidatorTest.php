<?php

namespace Advent\day4\v1\tests;

use Advent\day4\v1\Passport;
use Advent\day4\v1\Security;
use Advent\day4\v1\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{

    public static array $validPassport
        = [
            'ecl' => 'gry',
            'pid' => '860033327',
            'eyr' => '2020',
            'hcl' => '#fffffd',
            'byr' => '1937',
            'iyr' => '2017',
            'cid' => '147',
            'hgt' => '183cm',
        ];

    public static array $invalidPassport
        = [
            "iyr" => "2013",
            "ecl" => "amb",
            "cid" => "350",
            "eyr" => "2023",
            "pid" => "028048884",
            "hcl" => "#cfa07d",
            "byr" => "1929",
        ];

    public static array $specialPassport
        = [
            "hcl" => "#ae17e1",
            "iyr" => "2013",
            "eyr" => "2024",
            "ecl" => "brn",
            "pid" => "760753108",
            "byr" => "1931",
            "hgt" => "179cm",
        ];

    public function testInstance()
    {
        $security = new Validator();

        $this->assertInstanceOf('Advent\day4\v1\Validator', $security);
    }

    public function testPassportIsValid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$validPassport);

        $isValid = $validator->isValid($passport);

        $this->assertTrue($isValid);
    }

    public function testPassportIsInvalid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$invalidPassport);

        $isValid = $validator->isValid($passport);

        $this->assertFalse($isValid);
    }

    public function testPassportSpecialIsValid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$specialPassport);

        $isValid = $validator->isValid($passport);

        $this->assertTrue($isValid);
    }

}


<?php


namespace Advent\day4\v2;


class Passport
{

    public bool $valid = false;
    public bool $validated = false;
    public int $byr = 0; // birth year
    public int $iyr = 0; // issue year
    public int $eyr = 0; // expiration year
    public string $hgt = ''; // height
    public string $hcl = ''; // hair color
    public string $ecl = ''; // eye color
    public string $pid = ''; // passport id
    public string $cid = '';

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     */
    public function setValid(bool $valid): void
    {
        $this->valid = $valid;
    }

    /**
     * @return bool
     */
    public function getValid(): bool
    {
        return $this->valid;
    }

    /**
     * Passport constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $k => $row) {
            $method = $k;

           $this->{'set'.ucfirst(strtolower($method))}($row);
        }
    }

    /**
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     */
    public function setValidated(bool $validated): void
    {
        $this->validated = $validated;
    }

    /**
     * @return int
     */
    public function getByr(): int
    {
        return $this->byr;
    }

    /**
     * @param int $byr
     */
    public function setByr(int $byr): void
    {
        $this->byr = $byr;
    }

    /**
     * @return int
     */
    public function getIyr(): int
    {
        return $this->iyr;
    }

    /**
     * @param int $iyr
     */
    public function setIyr(int $iyr): void
    {
        $this->iyr = $iyr;
    }

    /**
     * @return int
     */
    public function getEyr(): int
    {
        return $this->eyr;
    }

    /**
     * @param int $eyr
     */
    public function setEyr(int $eyr): void
    {
        $this->eyr = $eyr;
    }

    /**
     * @return string
     */
    public function getHgt(): string
    {
        return $this->hgt;
    }

    /**
     * @param string $hgt
     */
    public function setHgt(string $hgt): void
    {
        $this->hgt = $hgt;
    }

    /**
     * @return string
     */
    public function getHcl(): string
    {
        return $this->hcl;
    }

    /**
     * @param string $hcl
     */
    public function setHcl(string $hcl): void
    {
        $this->hcl = $hcl;
    }

    /**
     * @return string
     */
    public function getEcl(): string
    {
        return $this->ecl;
    }

    /**
     * @param string $ecl
     */
    public function setEcl(string $ecl): void
    {
        $this->ecl = $ecl;
    }

    /**
     * @return string
     */
    public function getPid(): string
    {
        return $this->pid;
    }

    /**
     * @param string $pid
     */
    public function setPid(string $pid): void
    {
        $this->pid = $pid;
    }

    /**
     * @return string
     */
    public function getCid(): string
    {
        return $this->cid;
    }

    /**
     * @param string $cid
     */
    public function setCid(string $cid): void
    {
        $this->cid = $cid;
    }






    public static function createFromArray(array $data)
    {
        return new self($data);
    }

    public static function createFromString(string $dataString)
    {
        $data = [];

        $tmp = explode(' ', str_replace("\n", ' ', $dataString));
        foreach ($tmp as $k => $row) {
            $rowData = explode(':', $row);
            $data[$rowData[0]] = $rowData[1] ?? '';
        }
        
        return self::createFromArray($data);
    }

    public function markValidated()
    {
        $this->validated = true;
    }
}
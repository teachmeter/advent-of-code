<?php


namespace Advent\day4\v2;


use PHPUnit\Util\Exception;

class Security
{

    /**
     * @var array<<Passport>>
     */
    public array $passports = [];

    /**
     * @var ValidatorInterface
     */
    public ValidatorInterface $validator;

    /**
     * Security constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }


    public function explodeStringToPassportsStrings($raw) : array
    {
        return explode("\n\n", $raw);
    }

    public function setPassports($string) : void
    {
        $passportsArray = [];
        $passportsStrings = $this->explodeStringToPassportsStrings($string);

        foreach ($passportsStrings as $k => $passportString) {
            $passportsArray[] = Passport::createFromString($passportString);
        }

        $this->passports = $passportsArray;
    }

    public function getPassports() : array
    {
        return $this->passports;
    }


    public function validatePassports() : void
    {
        if (empty($this->passports)) {
            throw new Exception('Passports list empty');
        }
        /**
         * @var $passport Passport
         */
        foreach ($this->passports as $k => $passport) {
            $valid = $this->validatePassport($passport);
            $passport->setValid($valid);
            $passport->setValidated(true);

            $this->passports[$k] = $passport;
        }
    }

    public function validatePassport(Passport $passport)
    {
        return $this->validator->isValid($passport);
    }

    public function countValidPassports() : int
    {
        $validCount = 0;

        $passports = $this->getPassports();
        
        /**
         * @var $passport Passport
         */
        foreach ($passports as $k => $passport) {
            if ($passport->validated && $passport->getValid()) {
                

                $validCount += 1;
            }
        }

        return $validCount;
    }
}
<?php

namespace Advent\day4\v2;

class Validator implements ValidatorInterface
{
    public static array $required
        = [
            'byr',
            'iyr',
            'eyr',
            'hgt',
            'hcl',
            'ecl',
            'pid',
            //        'cid',
        ];

    public function isValid(Passport $passport): bool
    {
        return $this->checkHasAllFields($passport)
            && $this->checkBirthYearValid($passport->getByr())
            && $this->checkIssueYearValid($passport->getIyr())
            && $this->checkExpirationYearValid($passport->getEyr())
            && $this->checkHeightValid($passport->getHgt())
            && $this->checkHairColorValid($passport->getHcl())
            && $this->checkEyeColorValid($passport->getEcl())
            && $this->checkPIDValid($passport->getPid());
    }

    public function checkRequired($key, $object)
    {
        $value = $object->{'get' . ucfirst(strtolower($key))}();

        return isset($value) && ! empty($value);
    }

    public function checkHasAllFields(Passport $passport)
    {
        $validArray = [];

        foreach (self::$required as $k => $field_name) {
            $validArray[$field_name] = $this->checkRequired($field_name, $passport);
        }

        return (bool)array_product($validArray);
    }

    public function checkBirthYearValid($value)
    {
        $isInt = is_int($value);
        $digitCount = strlen($value) === 4;
        $isInRange = $value >= 1920 && $value <= 2002;

        return $isInRange && $isInt && $digitCount;
    }

    public function checkIssueYearValid($value)
    {
        $isInt = is_int($value);
        $digitCount = strlen($value) === 4;
        $isInRange = $value >= 2010 && $value <= 2020;

        return $isInRange && $isInt && $digitCount;
    }

    public function checkExpirationYearValid($value)
    {
        $isInt = is_int($value);
        $digitCount = strlen($value) === 4;
        $isInRange = $value >= 2020 && $value <= 2030;

        return $isInRange && $isInt && $digitCount;
    }

    public function checkHeightValid($string)
    {
        $isMatch = preg_match('/(\d+)(cm|in)/', $string, $matches);

        if ( ! $isMatch) {
            return false;
        }
        $validNumber = false;
        $height = $matches[1];

        if ($matches[2] == 'cm') {
            $validNumber = $height >= 150 && $height <= 193;
        } elseif ($matches[2] == 'in') {
            $validNumber = $height >= 59 && $height <= 76;
        } else {
            $validNumber = false;
        }

        return $isMatch && $validNumber;
    }

    public function checkHairColorValid($string)
    {
        $isMatch = preg_match('/^#([0-9a-f]{6})$/', $string);

        return (bool)$isMatch;
    }

    public function checkEyeColorValid($string)
    {
        $versions = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];

        return in_array($string, $versions);
    }

    public function checkPIDValid($string)
    {
        return preg_match('/\d{9}/', $string) && strlen($string) === 9;
    }

}
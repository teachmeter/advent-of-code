<?php


namespace Advent\day4\v2;


interface ValidatorInterface
{

    public function isValid(Passport $passport) : bool;
}
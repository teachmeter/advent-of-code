<?php

namespace Advent\day4\v2\tests;

use Advent\day4\v2\Passport;
use Advent\day4\v2\Validator;
use PHPUnit\Framework\TestCase;

class PassportTest extends TestCase
{

    public static array $validPassport
        = [
            'ecl' => 'gry',
            'pid' => '860033327',
            'eyr' => '2020',
            'hcl' => '#fffffd',
            'byr' => '1937',
            'iyr' => '2017',
            'cid' => '147',
            'hgt' => '183cm',
        ];

    public function testInstance()
    {
        $passport = new Passport([]);

        $this->assertInstanceOf('Advent\day4\v2\Passport', $passport);
    }

    public function testSettingVariables()
    {
        $passport = new Passport(self::$validPassport);

        $this->assertNotEmpty($passport->getByr());
        $this->assertEquals(self::$validPassport['byr'], $passport->getByr());

        $this->assertNotEmpty($passport->getIyr());
        $this->assertEquals(self::$validPassport['iyr'], $passport->getIyr());

        $this->assertNotEmpty($passport->getEyr());
        $this->assertEquals(self::$validPassport['eyr'], $passport->getEyr());

        $this->assertNotEmpty($passport->getHgt());
        $this->assertEquals(self::$validPassport['hgt'], $passport->getHgt());

        $this->assertNotEmpty($passport->getHcl());
        $this->assertEquals(self::$validPassport['hcl'], $passport->getHcl());

        $this->assertNotEmpty($passport->getEcl());
        $this->assertEquals(self::$validPassport['ecl'], $passport->getEcl());

        $this->assertNotEmpty($passport->getPid());
        $this->assertEquals(self::$validPassport['pid'], $passport->getPid());

        $this->assertNotEmpty($passport->getCid());
        $this->assertEquals(self::$validPassport['cid'], $passport->getCid());

    }
}


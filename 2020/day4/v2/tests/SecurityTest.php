<?php

namespace Advent\day4\v2\tests;

use Advent\day4\v2\Security;
use Advent\day4\v2\Validator;
use PHPUnit\Framework\TestCase;

class SecurityTest extends TestCase
{

    public static string $exampleInput = 'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in';

    public function testInstance()
    {
        $validator = new Validator();
        $security = new Security($validator);

        $this->assertInstanceOf('Advent\day4\v2\Security', $security);
    }

    public function testSetPassportsList()
    {
        $validator = new Validator();
        $security = new Security($validator);

        $passports = $security->explodeStringToPassportsStrings(self::$exampleInput);

        $this->assertCount(4, $passports);
    }

    public function testPassportsListInstances()
    {
        $validator = new Validator();
        $security = new Security($validator);

        $security->setPassports(self::$exampleInput);

        $this->assertContainsOnlyInstancesOf('Advent\day4\v2\Passport', $security->getPassports());
    }


}


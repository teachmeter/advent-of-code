<?php

namespace Advent\day4\v2\tests;

use Advent\day4\v2\Passport;
use Advent\day4\v2\Security;
use Advent\day4\v2\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{

    public static array $validPassport
        = [
            'ecl' => 'gry',
            'pid' => '860033327',
            'eyr' => '2020',
            'hcl' => '#fffffd',
            'byr' => '1937',
            'iyr' => '2017',
            'cid' => '147',
            'hgt' => '183cm',
        ];

    public static array $invalidPassport
        = [
            "iyr" => "2013",
            "ecl" => "amb",
            "cid" => "350",
            "eyr" => "2023",
            "pid" => "028048884",
            "hcl" => "#cfa07d",
            "byr" => "1929",
        ];

    public static array $specialPassport
        = [
            "hcl" => "#ae17e1",
            "iyr" => "2013",
            "eyr" => "2024",
            "ecl" => "brn",
            "pid" => "760753108",
            "byr" => "1931",
            "hgt" => "179cm",
        ];

    public function testInstance()
    {
        $security = new Validator();

        $this->assertInstanceOf('Advent\day4\v2\Validator', $security);
    }

    public function testPassportIsValid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$validPassport);

        $isValid = $validator->isValid($passport);

        $this->assertTrue($isValid);
    }

    public function testPassportIsInvalid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$invalidPassport);

        $isValid = $validator->isValid($passport);

        $this->assertFalse($isValid);
    }

    public function testPassportSpecialIsValid()
    {
        $validator = new Validator();

        $passport = Passport::createFromArray(self::$specialPassport);

        $isValid = $validator->isValid($passport);

        $this->assertTrue($isValid);
    }


    public function testBirthYear()
    {
        $validator = new Validator();

        $this->assertTrue($validator->checkBirthYearValid(2002));
        $this->assertFalse($validator->checkBirthYearValid(19888));
        $this->assertFalse($validator->checkBirthYearValid(198));
        $this->assertFalse($validator->checkBirthYearValid('1988'));
        $this->assertFalse($validator->checkBirthYearValid(1919));
        $this->assertFalse($validator->checkBirthYearValid(2003));
    }


    public function testIssueYear()
    {
        $validator = new Validator();

        $this->assertTrue($validator->checkIssueYearValid(2011));
        $this->assertFalse($validator->checkIssueYearValid(2009));
        $this->assertFalse($validator->checkIssueYearValid(2021));
        $this->assertFalse($validator->checkIssueYearValid(123));
        $this->assertFalse($validator->checkIssueYearValid(12345));
        $this->assertFalse($validator->checkIssueYearValid('2011'));
    }

    public function testExpirationYear()
    {
        $validator = new Validator();

        $this->assertTrue($validator->checkExpirationYearValid(2025));
        $this->assertFalse($validator->checkExpirationYearValid(2019));
        $this->assertFalse($validator->checkExpirationYearValid(2031));

        $this->assertFalse($validator->checkExpirationYearValid(123));
        $this->assertFalse($validator->checkExpirationYearValid(12345));
    }

    public function testHeight()
    {
        $validator = new Validator();


        $this->assertFalse($validator->checkHeightValid('123'));
        $this->assertFalse($validator->checkHeightValid('149cm'));
        $this->assertFalse($validator->checkHeightValid('194cm'));
        $this->assertTrue($validator->checkHeightValid('183cm'));

        $this->assertFalse($validator->checkHeightValid('58in'));
        $this->assertFalse($validator->checkHeightValid('190in'));
        $this->assertTrue($validator->checkHeightValid('60in'));
    }

    public function testEyeColor()
    {
        $validator = new Validator();

        $versions = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];
        foreach ($versions as $k => $color) {
            $this->assertTrue($validator->checkEyeColorValid($color));
        }

        $this->assertFalse($validator->checkHeightValid('ddd'));
    }

    public function testPid()
    {
        $validator = new Validator();

        $this->assertTrue($validator->checkPIDValid('000099123'));
        $this->assertFalse($validator->checkPIDValid('00099123'));
        $this->assertFalse($validator->checkPIDValid('0000a9123'));
        $this->assertFalse($validator->checkPIDValid('0000a91231'));
    }

    public function testInvalidPassports()
    {
        $inputString = 'eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007';

        $validator = new Validator();
        $security = new Security($validator);
        $security->setPassports($inputString);

        $passports = $security->getPassports();

        foreach ($passports as $k => $passport) {
            $this->assertFalse($validator->isValid($passport), $passport->getPid());
        }
    }

    public function testValidPassports()
    {
        $inputString = 'pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719';

        $validator = new Validator();
        $security = new Security($validator);
        $security->setPassports($inputString);

        $passports = $security->getPassports();

        foreach ($passports as $k => $passport) {
            $this->assertTrue($validator->isValid($passport), $passport->getPid());
        }
    }

}


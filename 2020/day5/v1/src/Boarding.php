<?php


namespace Advent\day5\v1;


class Boarding
{
    CONST TOTAL_ROWS = 128;

    public array $availablePlaces = [];
    public array $columns = [0,1,2,3,4,5,6,7];

    /**
     * Boarding constructor.
     */
    public function __construct()
    {
        $this->generateAllPlaces();
    }


    public function generateAllPlaces()
    {
        $rowPlaces = [];

        for($i=0; $i< self::TOTAL_ROWS; $i++) {
            $rowPlaces[$i] = $this->columns;
        }

        $this->availablePlaces = $rowPlaces;
    }

    public function getUuid($row, $column)
    {
        return ($row*8) + $column;
    }

    public function handleF(array $places)
    {
        $keys = array_keys($places);

        $bottom = $keys[0];

        $length = ceil(count($places) / 2);
        $newPlaces = array_slice($places, 0, $length , true);

        return $newPlaces;
    }

    public function handleB(array $places)
    {
        $keys = array_keys($places);

        $bottom = ceil(count($keys) / 2);

        $newPlaces = array_slice($places, $bottom, count($keys), true);

        return $newPlaces;
    }

    public function handleR(array $columns)
    {
        return array_slice($columns, ceil(count($columns)/2), null, true);
    }

    public function handleL(array $columns)
    {
        return array_slice($columns, 0, ceil(count($columns) /2), true);
    }
}
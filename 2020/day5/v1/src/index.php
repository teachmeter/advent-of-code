<?php 

require_once __DIR__ . '/../vendor/autoload.php';

$uuids = [];
echo "<pre>";

$input = file_get_contents(__DIR__ .'/input.txt');

$strings = explode("\n", $input);

foreach ($strings as $k => $string) {
    $uuids[] = getUUid($string);
}


//print_r(max($uuids));

$tmpUuids = [];
$b = new \Advent\day5\v1\Boarding();
for ($i=0; $i<128;$i++) {
    for($j=0; $j<8; $j++) {
        $tmpUuids[] = $b->getUuid($i, $j);
    }
}
sort($uuids);
//var_dump($uuids);
//var_dump($tmpUuids);
print_r(array_diff($tmpUuids, $uuids));


echo "</pre>";

function getUUid($string) {
    $boarding = new \Advent\day5\v1\Boarding();

    $places = $boarding->availablePlaces;
    $actionIndex = str_split($string);

    $rowsActions = array_slice($actionIndex, 0, -3);
    $columnsActions = array_slice($actionIndex, count($actionIndex) - 3);

    $rowsActionsPosition = 0;
    while(count($places) > 1) {
        $action = $rowsActions[$rowsActionsPosition];
        if ($action == 'F') {
            $places = $boarding->handleF($places);
        } elseif($action == 'B') {
            $places = $boarding->handleB($places);
        }
        $rowsActionsPosition++;
    }
    $row = array_key_first($places);
    $columns = current($places);

    $columnsActionPosition = 0;
    while(count($columns) > 1) {
        $action = $columnsActions[$columnsActionPosition];
        if ($action == 'R') {
            $columns = $boarding->handleR($columns);
        } elseif($action == 'L') {
            $columns = $boarding->handleL($columns);
        }
        $columnsActionPosition++;
    }
    $column = array_key_first($columns);
    return $boarding->getUuid($row, $column);
}

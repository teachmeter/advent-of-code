<?php

namespace Advent\day5\v1\tests;

use Advent\day5\v1\Boarding;
use PHPUnit\Framework\TestCase;

class BoardingTest extends TestCase
{

    public function testInstance()
    {
        $boarding = new Boarding();

        $this->assertInstanceOf('Advent\day5\v1\Boarding', $boarding);
    }

    public function testUuid()
    {
        $boarding = new Boarding();

        $this->assertEquals(357, $boarding->getUuid(44, 5));
    }
}

<?php
$input = file_get_contents(__DIR__ . '/input.txt');
$exampleString = 'abc

a
b
c

ab
ac

a
a
a
a

b';

//$groups = explode("\n\n", $exampleString);
$groups = explode("\n\n", $input);

$answers = [];
foreach ($groups as $k => $groupString) {

    $subGroup = explode("\n", $groupString);

    foreach ($subGroup as $m => $sub) {
        $subGroup[$m] = str_split($sub);
    }
    $answers[$k] = $subGroup;
}

$countTotal = 0;
foreach ($answers as $groupId => $groupAnswers) {
    $localCount = 0;

    if (count($groupAnswers) < 2) {
        $countTotal += count($groupAnswers[0]);
    } else {
        $countTotal += count(call_user_func_array('array_intersect', $groupAnswers));
    }
}

print_r($countTotal);

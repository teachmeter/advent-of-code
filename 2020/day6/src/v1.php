<?php
echo "<pre>";
$input = file_get_contents(__DIR__ . '/input.txt');
$exampleString = 'abc

a
b
c

ab
ac

a
a
a
a

b';

$groups = explode("\n\n", $input);

$answers = [];
foreach ($groups as $k => $groupString) {
    $groupString = str_replace([' ', "\n"], '', $groupString);
    $answers[$k] = count(array_unique(str_split($groupString)));
}

print_r(array_sum($answers));
echo "</pre>";
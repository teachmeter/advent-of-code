<?php


namespace Advent\day7\v1;


class Rule
{
    public string $id = '';
    public array $allowedColors = [];

    /**
     * Rule constructor.
     *
     * @param string $id
     * @param array  $allowedColors
     */
    public function __construct(string $id, array $allowedColors)
    {
        $this->id = $id;
        $this->allowedColors = $allowedColors;
    }

    public static function createFromString(string $rulesString): Rule
    {
        $tmp = explode(' contain ', $rulesString);

        $id = trim(str_replace(['bags', 'bag', '.'], '', $tmp[0]));

        $allowedColors = array_map(
            function ($item) {
                return trim(substr($item, 2));
            },
            explode(', ', str_replace(['.', 'bags', 'bag'], '', $tmp[1]))
        );


        return new self($id, $allowedColors);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getAllowedColors(): array
    {
        return $this->allowedColors;
    }


    public function isAllowed(string $bagColor) : bool
    {
        return in_array($bagColor, $this->getAllowedColors());
    }

}
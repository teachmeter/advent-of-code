<?php

require_once __DIR__ . '/../vendor/autoload.php';

//$rulesString = file_get_contents(__DIR__ . '/input.txt');
$rulesString = 'light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.';

$rules = \Advent\day7\v1\Rules::createRulesFromString($rulesString);
$testedIds = [];
$queue = ['shiny gold'];
$testedNeedels = [];

while ( ! empty($queue)) {
    $needle = array_shift($queue);
    $testedNeedels[] = $needle;

    $testedIdsStack = $rules->canHaveBag($needle);
    if ( ! empty($testedIdsStack)) {
        foreach ($testedIdsStack as $k => $item) {
            $queue[] = $item->getId();
            $testedIds[] = $item->getId();
        }

        foreach ($testedIdsStack as $k => $item) {
            $rules->setRules($rules->removeRule($item->getId()));
        }
    }
}

echo "<pre>";
echo sprintf('found: %d', count($testedIds)) . '<br>';
print_r($testedIds);
echo "</pre>";

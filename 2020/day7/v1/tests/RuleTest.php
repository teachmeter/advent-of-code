<?php

namespace Advent\day7\v1\tests;

use Advent\day7\v1\Rule;
use PHPUnit\Framework\TestCase;

class RuleTest extends TestCase
{

    public function testInstance()
    {
        $rule = Rule::createFromString('light red bags contain 1 bright white bag, 2 muted yellow bags.');

        $this->assertInstanceOf('Advent\day7\v1\Rule', $rule);
    }

    public function testParametersSet()
    {
        $rule = Rule::createFromString('light red bags contain 1 bright white bag, 2 muted yellow bags.');

        $this->assertEquals('light red', $rule->getId());
        $this->assertEquals(['bright white', 'muted yellow'], $rule->getAllowedColors());
    }

    public function checkBagIsAllowed()
    {
        $rule = Rule::createFromString('light red bags contain 1 bright white bag, 2 muted yellow bags.');

        $this->assertTrue($rule->isAllowed('muted yellow'));
    }

    public function checkBagIsNotAllowed()
    {
        $rule = Rule::createFromString('light red bags contain 1 bright white bag, 2 muted yellow bags.');

        $this->assertTrue($rule->isAllowed('bright yellow'));
    }
}

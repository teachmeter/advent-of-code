<?php


namespace Advent\day7\v2;


class Rule
{
    public string $id = '';
    public array $allowedColors = [];
    public array $allowedCounts = [];

    /**
     * Rule constructor.
     *
     * @param string $id
     * @param array  $allowedColors
     */
    public function __construct(string $id, array $allowedColors, array $allowedCounts)
    {
        $this->id = $id;
        $this->allowedColors = $allowedColors;
        $this->allowedCounts = $allowedCounts;
    }

    public static function createFromString(string $rulesString): Rule
    {
        $tmp = explode(' bags contain ', $rulesString);

        $id = trim(str_replace(['bag', '.'], '', $tmp[0]));

        $colorsTmp = explode(', ', str_replace(['.', 'bags', 'bag'], '', $tmp[1]));
        $allowedColors = array_map(
            function ($item) {
                return trim(substr($item, 2));
            },
            $colorsTmp
        );
        $allowedCounts = [];
        foreach ($colorsTmp as $k => $color) {
            $allowedCounts[trim(substr($color, 2))] = (int) (explode(' ', $color))[0];
        }

        return new self($id, $allowedColors, $allowedCounts);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getAllowedColors(): array
    {
        return $this->allowedColors;
    }


    public function isAllowed(string $bagColor) : bool
    {
        return in_array($bagColor, $this->getAllowedColors());
    }

    public function getCountForSubSet($id) : int
    {
        foreach ($this->allowedCounts as $key => $count) {
            if ($id == $key) {
                return $count;
            }
        }
        return 0;
    }

}
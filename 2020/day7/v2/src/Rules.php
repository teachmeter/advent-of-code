<?php


namespace Advent\day7\v2;


use PHPUnit\Util\Exception;

class Rules
{
    protected array $rules = [];

    /**
     * Rules constructor.
     *
     * @param array<<Rule>> $rules
     */
    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    public static function createRulesFromString(string $rules) : Rules
    {
        $rulesArray = [];
        $rulesStrings = explode("\n", $rules);

        foreach ($rulesStrings as $k => $rulesString) {
            $rulesArray[] = Rule::createFromString($rulesString);
        }

        return new self($rulesArray);
    }

    public static function createFromRulesObjectsArray(array $rules)
    {
        return new self($rules);
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }


    /**
     * @param string $id
     *
     * @throws \Exception
     * @return Rule
     */
    public function getRule(string $id) : Rule
    {
        /**
         * @var $rule Rule
         */
        foreach ($this->getRules() as $k => $rule) {
            if ($rule->getId() === $id) {
                return $rule;
            }
        }

        throw new Exception('Rule not found');
    }

    public function canHaveBag(string $id)
    {
        $stack = [];

        /**
         * @var Rule $rule
         */
        foreach ($this->getRules() as $k => $rule) {
            if ( $rule->isAllowed($id) ) {
                $stack[] = $rule;
            }
        }

        return $stack;
    }


    public function removeRule(string $id)
    {
        $localRules = $this->getRules();
        foreach ($localRules as $k => $rule) {
            if($id == $rule->getId())
            {
                unset($localRules[$k]);
            }
        }

        $this->rules = $localRules;

        return $localRules;
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules): void
    {
        $this->rules = $rules;
    }


    public function countBagsInside($color)
    {
        $total = 0;
        try {
            $rule = $this->getRule($color);
            if($rule && $rule->allowedCounts) {
                foreach ($rule->allowedCounts as $id => $count) {
                    $total += $count + ($count * $this->countBagsInside($id));
                }
            }
        } catch (\Exception $e) {
            return 0;
        }


        return $total;
    }



}
<?php

require_once __DIR__ . '/../vendor/autoload.php';

$rulesString = file_get_contents(__DIR__ . '/input.txt');
$rulesString1 = 'light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.';
echo "<pre>";
$rules = \Advent\day7\v2\Rules::createRulesFromString($rulesString);
$testedIds = [];
$queue = ['shiny gold'];
$testedNeedels = [];
$totalCount = 0;

$stack = [];
//
//while ( ! empty($queue)) {
//    $needle = array_shift($queue);
//
//    try {
//        $rule = $rules->getRule($needle);
//        var_dump($rule);
//        $newNeedles = array_keys($rule->allowedCounts);
//
//        foreach ($newNeedles as $k => $n) {
//            $queue[] = $n;
//            $totalCount += $rule->allowedCounts[$n];
//            if (isset($stack[$n])) {
//                $stack[$n] += $rule->allowedCounts[$n];
//            } else {
//                $stack[$n] = $rule->allowedCounts[$n];
//            }
//
//        }
//    } catch (\Exception $e) {
//        continue;
//    }
//
//}
$totalCount = $rules->countBagsInside('shiny gold');

echo sprintf('total bags: %d', $totalCount) . '<br>';
echo "</pre>";

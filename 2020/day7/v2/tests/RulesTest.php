<?php

namespace Advent\day7\v2\tests;

use Advent\day7\v2\Rules;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Exception;

class RulesTest extends TestCase
{
    public const RULES = 'light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.';

    public function testInstance()
    {
        $rules = Rules::createRulesFromString(self::RULES);

        $this->assertInstanceOf('Advent\day7\v2\Rules', $rules);
    }

    public function testInstanceOfRules()
    {
        $rules = Rules::createRulesFromString(self::RULES);

        $this->assertContainsOnlyInstancesOf('Advent\day7\v2\Rule', $rules->getRules());
    }

    public function testCountOfRules()
    {
        $rules = Rules::createRulesFromString(self::RULES);

        $this->assertCount(9, $rules->getRules());
    }

    public function testFindsRule()
    {
        $rules = Rules::createRulesFromString(self::RULES);

        $rule = $rules->getRule('dark olive');

        $this->assertNotEmpty($rule);
        $this->assertInstanceOf('Advent\day7\v2\Rule', $rule);
    }

    /**
     * @expectedException
     */
    public function testThrowsExceptionIfRuleNotFound()
    {

        $rules = Rules::createRulesFromString(self::RULES);

        $this->expectException(Exception::class);

        $rules->getRule('pink olive');


    }
}

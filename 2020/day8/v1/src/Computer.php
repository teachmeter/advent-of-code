<?php


namespace Advent\day8\v1;


class Computer
{
    public array $registry = [];

    public int $accumulator = 0;

    public array $instructions = [];

    public int $currentIndex = 0;

    public bool $power = false;

    public bool $succesfullBoot = false;

    public function trigger(string $eventName)
    {
        if ($eventName == 'next') {
            $this->bumpIndex();
        }
    }

    /**
     * @return bool
     */
    public function getPower(): bool
    {
        return $this->power;
    }

    /**
     * @param bool $power
     */
    public function setPower(bool $power): void
    {
        $this->power = $power;
    }

    /**
     * @param bool $succesfullBoot
     */
    public function setSuccesfullBoot(bool $succesfullBoot): void
    {
        $this->succesfullBoot = $succesfullBoot;
    }



    public function checkSuccessfullBoot()
    {
        $instructionsCount = count($this->getInstructions());
        if(in_array($instructionsCount - 1, $this->getRegistry()))
        {
            $this->setSuccesfullBoot(true);
            $this->setPower(false);
        }
    }

    public function addIndexToRegistry($index)
    {
        $this->registry[] = $index;
    }

    public function getRegistry()
    {
        return $this->registry;
    }

    public function checkIndexInRegistry($index)
    {
        return in_array($index, $this->getRegistry());
    }

    /**
     * @param int $currentIndex
     */
    public function setCurrentIndex(int $currentIndex): void
    {
        $this->currentIndex = $currentIndex;
    }

    public function getCurrentIndex(): int
    {
        return $this->currentIndex;
    }


    private function bumpIndex()
    {
        $this->setCurrentIndex($this->getCurrentIndex() + 1);
    }

    public function getAccumulator()
    {
        return $this->accumulator;
    }

    public function acc(string $value)
    {
        $this->accumulator += intval($value);
        $this->trigger('next');

    }

    public function jmp(string $value)
    {
        $newIndex = $this->getCurrentIndex() + intval($value);
        $this->setCurrentIndex($newIndex);
    }

    public function nop()
    {
        $this->trigger('next');
    }

    public function processInstruction(string $instruction)
    {
        list($method, $value) = explode(' ', $instruction);


        if ($this->checkIndexInRegistry($this->getCurrentIndex())) {
            $this->setPower(false);
        } else {
            $this->addIndexToRegistry($this->getCurrentIndex());
            $this->{$method}($value);
        }

        $this->checkSuccessfullBoot();
    }

    public function processInstructions()
    {
       $this->processInstruction( ($this->getInstructions())[$this->getCurrentIndex()] );
    }

    public function setInstructions(array $instructions)
    {
        $this->instructions = $instructions;
    }

    public function getInstructions()
    {
        return $this->instructions;
    }

    public function parseInput(string $input)
    {
        $instructions = explode("\n", $input);

        $this->setInstructions($instructions);
    }

    public function run()
    {
        $this->setPower(true);

        while ($this->getPower() && $this->succesfullBoot == false) {
            $this->processInstructions();
        }
    }
}
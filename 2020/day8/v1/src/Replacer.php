<?php


namespace Advent\day8\v1;


class Replacer
{
    public string $instructionsString = '';

    public int $replaceIndex = 0;

    public string $mode = 'jmp';

    /**
     * Replacer constructor.
     *
     * @param string $instructionsString
     */
    public function __construct(string $instructionsString)
    {
        $this->instructionsString = $instructionsString;
    }

    public function getFixedInstructions($index, $mode = 'jmp')
    {
        $instructions = explode("\n", $this->instructionsString);
        $instructions[$index] = $this->replaceLine($mode, $instructions[$index]);
        return implode("\n", $instructions);
    }

    public function replaceLine($mode, $string)
    {
        if ($mode == 'jmp') {
            return str_replace('jmp', 'nop', $string);
        } elseif($mode == 'nop') {
            return str_replace('nop', 'jmp', $string);
        }
    }
}
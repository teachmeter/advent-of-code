<?php 
require_once __DIR__ . '/../vendor/autoload.php';
echo "<pre>";
$input = 'nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6';

$input = file_get_contents(__DIR__ . '/../../input.txt');



$n = 0;
$jmpIndexes = array_keys(array_filter(explode("\n", $input), function($item, $key){
    if (strpos($item, 'jmp') !== false) {
        return true;
    } else {
        return false;
    }
}, ARRAY_FILTER_USE_BOTH));

$nopIndexes = array_keys(array_filter(explode("\n", $input), function($item, $key){
    if (strpos($item, 'nop') !== false) {
        return true;
    } else {
        return false;
    }
}, ARRAY_FILTER_USE_BOTH));


$instructionsSets = [];

foreach ($nopIndexes as $i) {
    $instructionsSets[] = (new \Advent\day8\v1\Replacer($input))->getFixedInstructions($i, 'nop');
}

foreach ($jmpIndexes as $j) {
    $instructionsSets[] = (new \Advent\day8\v1\Replacer($input))->getFixedInstructions($j, 'jmp');
}


foreach ($instructionsSets as $k => $input) {
    $computer = new \Advent\day8\v1\Computer();
    $computer->parseInput($input);
    $computer->run();
    $computer->checkSuccessfullBoot();
    $boot = $computer->succesfullBoot;
    echo "<br>";
    echo "-----";
    echo "<br>";
    echo sprintf('successfull boot: %s', $boot ? 'true' : 'false');
    echo "<br>";
    echo sprintf('accumulator: %d', $computer->getAccumulator());
    echo "<br>";
    echo "-----";
    echo "<br>";
}

echo "</pre>";


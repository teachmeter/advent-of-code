<?php


namespace Advent\day8\v1\tests;


use Advent\day8\v1\Computer;

class ComputerTest extends \PHPUnit\Framework\TestCase
{

    public function testComputerInstance()
    {
        $comp = new Computer();

        $this->assertInstanceOf('Advent\day8\v1\Computer', $comp);
    }

    public function testMethodAcc()
    {
        $comp = new Computer();

        $this->assertEquals(0, $comp->getAccumulator());

        $comp->acc('+5');
        $this->assertEquals(5, $comp->getAccumulator());
    }

    public function testMethodJmp()
    {
        $comp = new Computer();

        $this->assertEquals(0, $comp->getCurrentIndex());

        $comp->jmp('+5');
        $this->assertEquals(5, $comp->getCurrentIndex());
        $comp->jmp('+1');
        $this->assertEquals(6, $comp->getCurrentIndex());
        $comp->jmp('-1');
        $this->assertEquals(5, $comp->getCurrentIndex());
    }

    public function testEventNext()
    {
        $comp = new Computer();

        $this->assertEquals(0, $comp->getCurrentIndex());
        $comp->trigger('next');
        $comp->trigger('next');
        $this->assertEquals(2, $comp->getCurrentIndex());

    }
}
<?php


namespace Advent\day9\v1;


class Xmas
{

    public array $preamble = [];

    public int $preambleLimit = 5;
    public int $index = 5;
    public int $preambleOffset = 0;

    /**
     * Xmas constructor.
     *
     * @param string $preambleString
     * @param int   $preambleLimit
     */
    public function __construct(string $preambleString = '', int $preambleLimit = 5)
    {
        $this->setPreamble(self::parsePreambleStringToArray($preambleString));
        $this->setPreambleLimit($preambleLimit);
        $this->setIndex($this->getPreambleLimit());
    }

    /**
     * @return int
     */
    public function getPreambleOffset(): int
    {
        return $this->preambleOffset;
    }

    /**
     * @param int $preambleOffset
     */
    public function setPreambleOffset(int $preambleOffset): void
    {
        $this->preambleOffset = $preambleOffset;
    }

    /**
     * @return array
     */
    public function getPreamble(): array
    {
        return $this->preamble;
    }

    /**
     * @param array $preamble
     */
    public function setPreamble(array $preamble): void
    {
        $this->preamble = $preamble;
    }

    /**
     * @return int
     */
    public function getPreambleLimit(): int
    {
        return $this->preambleLimit;
    }

    /**
     * @param int $preambleLimit
     */
    public function setPreambleLimit(int $preambleLimit): void
    {
        $this->preambleLimit = $preambleLimit;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex(int $index): void
    {
        $this->index = $index;
    }

    public static function parsePreambleStringToArray(string $numbers) : array
    {
        if (empty($numbers)) {
            return [];
        }

        $tmp = explode("\n", $numbers);
        $numbers = [];

        foreach ($tmp as $k => $item) {
            $numbers[] = (int) $item;
        }
        return $numbers;
    }

    public function getCurrentSubset()
    {
        $stack = $this->getPreamble();
        $limit = $this->getPreambleLimit();
        $offset = $this->index - $limit;
        
        return $this->getSubset($stack, $offset, $limit);
    }

    public function getSubset(array $stack, int $offset, int $limit)
    {
        return array_slice($stack, $offset, $limit);
    }

    public function advance()
    {
        $this->setPreambleOffset($this->getPreambleOffset() + 1);
        $this->setIndex($this->getIndex() + 1);
    }

    public function createSumArray(array $set) : array
    {
        // reset the keys for iteration

        $set = array_values($set);
        $limit = count($set);

        $sums = [];

        for ($i=0; $i<$limit;$i++) {
            for($j=$i+1; $j<$limit;$j++) {
                $sums[] = $set[$i] + $set[$j];
            }
        }

        sort($sums);

        return $sums;
    }

    public function isInArray($number, $sumsSet)
    {
        return in_array($number, $sumsSet);
    }

    public function isCurrentNumberValid()
    {
        $set = $this->getPreamble();
        $subset = $this->getSubset($this->getPreamble(), $this->getPreambleOffset(), $this->getPreambleLimit());

        return $this->isInArray($set[$this->getIndex()], $this->createSumArray($subset));
    }

    public function getCurrentNumber()
    {
        $set = $set = $this->getPreamble();
        return $set[$this->getIndex()];
    }
}
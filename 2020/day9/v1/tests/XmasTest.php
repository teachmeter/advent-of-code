<?php

namespace Advent\day8\v1\tests;

use Advent\day9\v1\Xmas;
use PHPUnit\Framework\TestCase;

class XmasTest extends TestCase
{

    public static string $exampleInput = '35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576';


    public function testInstance()
    {
        $xmas = new Xmas();

        $this->assertInstanceOf('Advent\day9\v1\Xmas', $xmas);
    }

    public function testParsePreambleString()
    {
        $xmas = new Xmas();

        $preamble = $xmas::parsePreambleStringToArray(self::$exampleInput);

        $this->assertIsArray($preamble);
        $this->assertNotEmpty($preamble);
        $this->assertCount(20, $preamble);
        $this->assertTrue(in_array(127, $preamble));
    }

    public function testGetSubset()
    {
        $xmas = new Xmas();

        $xmas->setPreamble($xmas::parsePreambleStringToArray(self::$exampleInput));
        $xmas->setPreambleLimit(5);
        $xmas->setPreambleOffset(0);

        $subset = $xmas->getSubset($xmas->getPreamble(), 0, 5);

        $this->assertEquals([35,20,15,25,47], $subset);
    }

    public function testGetCurrentSubset()
    {
        $xmas = new Xmas();

        $xmas->setPreamble($xmas::parsePreambleStringToArray(self::$exampleInput));
        $xmas->setPreambleOffset(1);
        $xmas->setIndex(6);
        $xmas->setPreambleLimit(5);

        $subset = $xmas->getCurrentSubset();

        $this->assertEquals([20,15,25,47,40], $subset);
    }

    public function testAdvance()
    {
        $xmas = new Xmas();

        $xmas->setPreamble($xmas::parsePreambleStringToArray(self::$exampleInput));
        $xmas->setPreambleOffset(1);
        $xmas->setIndex(6);

        $xmas->advance();

        $this->assertEquals(2, $xmas->getPreambleOffset());
        $this->assertEquals(7, $xmas->getIndex());

    }

    public function testCreateSums()
    {
        $set = [1,2,3];
        $expected = [3,4,5];

        $xmas = new Xmas();

        $sums = $xmas->createSumArray($set);

        $this->assertEquals($expected, $sums);

    }


}
